import queue

class arbol_profundidad:
    graph = {
              'A' : ['B','C'],
              'B' : ['D', 'E'],
              'C' : ['H','I'],
              'D' : ['F','G'],
              'E' : [],
              'F' : [],
              'G' : ['M','N'],
              'H' : [],
              'I' : ['J'],
              'J' : ['K','L'],
              'K' : [],
              'L' : [],
              'M' : [],
              'N' : []
            }

    graphdfs = {
              'A' : ['B' ,'C'],
              'B' : ['D', 'E'],
              'C' : ['F'],
              'D' : ['G'],
              'E' : ['F'],
              'F' : [],
              'G' : []
              
              
            }

    grafocosto = {
              'A' : {'B':1, 'C':2},
              'B' : {'D':3, 'E':2},
              'C' : {'F':0},
              'D' : {'G':0},
              'E' : {'F':0},
              'F' : {},
              'G' : {}
              
              
            }
    visited = []
    visited2 = [] 
    queue = []
    costo = 0
    paths=[]
    pathcostTotal=[]
    def __init__(self):
       
        #DFS
        print("DFS")
        self.dfs(self.visited,self.graphdfs,'A')
        print("DFS_PATH", self.visited)


        #BFS
        print("BFS")
        self.bfs(self.visited2,self.graph,'A')
        print("PATH_BFS",self.visited2)
        # UCS
        print("UCS")
        self.dfs_busqueda(self.grafocosto,'A','E',[],[],0)
        print("Path UCS: ", self.paths)
        self.imprimir_costo()
        
    def bfs(self,visited, graph, node):
        
        visited.append(node)
        self.queue.append(node)
        #s = self.queue.pop(0)
        #print(s)
        #print(self.graph[s])
        while self.queue:
            s = self.queue.pop(0)
            #print (s, end = " ")
            for neighbour in graph[s]:
                if neighbour not in visited:
                    visited.append(neighbour)
                    self.queue.append(neighbour)
                  

    def dfs(self, visited, graphdfs, node):
        if node not in visited:
            #print(node , end = " ")
            visited.append(node)
            for vecino in graphdfs[node]:
                self.dfs(visited,graphdfs,vecino)
            return visited
    

    def dfs_busqueda(self, grafo,start,end,path,pathcost,valueCost):
       
        path = path+[start]
        pathcost = pathcost+[valueCost]
       
        if start == end:
            
            self.paths.append(path)
            self.pathcostTotal.append(pathcost)
            return self.paths,self.pathcostTotal
      
        if start in grafo.keys():
            #print(start)
            nodos = grafo.get(start)
            #print("nodo ",nodos)
            for i in nodos:
                z = nodos
                #print ("Valor i " , i)
                if i not in path:
                    
                    #print("Costo",z[i])
                    cost = z[i]
                    self.dfs_busqueda(grafo,i,end,path,pathcost,cost)
      

        return self.paths,self.pathcostTotal


        
      
    def imprimir_costo(self):
       for valor in self.pathcostTotal:
            print("Costo Path: ", sum(valor))
                
            


pruebaArbol = arbol_profundidad()




    

    
